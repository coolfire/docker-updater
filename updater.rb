#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

compose_cmd = system('which docker-compose > /dev/null 2>&1') ? 'docker-compose' : 'docker compose'
parent_dir  = '/srv/docker-deployments'
restart_cmd = "#{compose_cmd} up -d --no-deps"
pull_cmd    = "#{compose_cmd} pull"

Dir.chdir parent_dir
directories = Dir.glob('*').select { |f| File.directory? f }

directories.each do |d|
  puts "--- #{d} ---"
  deployment = "#{parent_dir}/#{d}"

  compose_files = ['docker-compose.yaml', 'docker-compose.yml', 'compose.yaml', 'compose.yml']

  found_file = compose_files.find do |compose_file|
    File.exist?(File.join(deployment, compose_file))
  end

  if found_file.nil?
    puts 'No compose file found. Skipping.'
    next
  end

  Dir.chdir deployment

  puts `#{pull_cmd}`

  puts `#{restart_cmd}`
end
